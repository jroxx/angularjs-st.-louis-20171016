# Taste of Angular 2+

This directory contains an example Angular 2+ program, the "tour of
heroes" also available at the Angular website angular.io. You can
experiment with it online:

https://angular.io/docs/ts/latest/tutorial/

https://embed.plnkr.co/?show=preview

Or by running a Web server to serve the contents of this directory. For
example live-server can be used:

```
npm install -g live-server
live-server
```

Because this example is intended for online demo use, it actually
requests all of its libraries (in some cases, one file at a time)
online. This is *not* representative of a real Angular 2+ development
workflow.

## Preparing for Angular 2+

### Learn:

* Learn TypeScript; use it with A1.
* Learn a module loader (JSPM, Webpack); *maybe* use it for A1

### Code:

* Use data binding mostly in one direction.

### Big Apps

Angular Connect 2015, talk on applicaitons that scale

https://www.youtube.com/watch?v=eel3mV0alEc

https://docs.google.com/presentation/d/1P6214jYEi8d_dxqXdv9X6daJnr4K_BXgDnt8SwyTBwQ/edit#slide=id.gcc0320003_0_415
