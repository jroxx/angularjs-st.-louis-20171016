(function (angular) {
  'use strict';

  class CountryFetcher {
    constructor($http) {
      this.$http = $http;
    }

    countryList() {
      return this.$http.get('../demo-data/countries.json')
        .then(response => response.data);
    }
  }

  angular.module('countryList')
    .service('countryFetcher', CountryFetcher);

}(angular));
