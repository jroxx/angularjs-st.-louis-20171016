(function (angular) {
  'use strict';

  class CountryListDashboardController {
    constructor(countryFetcher) {
      countryFetcher.countryList()
        .then(list => this.countries = list);

      this.orderProp = 'countryName';
    }
  }

  angular.module('countryList')
    .component('countryListDashboard', {
      templateUrl: 'country-list/country-list-dashboard/country-list-dashboard.component.html',
      controller: CountryListDashboardController
    });

}(angular));
