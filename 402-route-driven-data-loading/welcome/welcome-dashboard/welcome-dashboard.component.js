(function (angular) {
  'use strict';

  angular.module('welcome')
    .component('welcomeDashboard', {
      templateUrl: 'welcome/welcome-dashboard/welcome-dashboard.component.html'
    });

}(angular));
