(function (angular) {
  'use strict';

  angular.module('app', [
    'ngRoute',
    'welcome',
    'countryList',
    'countryDetail'
  ])
    .config(($routeProvider, $locationProvider) => {
      // $locationProvider.html5Mode(true);
      $routeProvider
        .when('/welcome', {
          template: '<welcome-dashboard></welcome-dashboard>',
        })
        .when('/countries', {
          template: '<country-list-dashboard></country-list-dashboard>'
        })
        .when('/country/:countryCode', {
          template: '<country-detail-dashboard></country-detail-dashboard>'
        })
        .otherwise({
          redirectTo: '/welcome'
        });
    });

}(angular));
