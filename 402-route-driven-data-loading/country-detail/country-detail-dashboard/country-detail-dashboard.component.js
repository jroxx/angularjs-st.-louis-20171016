(function (angular) {
  'use strict';

  class CountryDetailDashboardController {
    constructor($routeParams, detailFetcher) {
      this.countryDetails = undefined;
      this.countryCode = $routeParams.countryCode;

      detailFetcher.load(this.countryCode)
        .then(countryData => this.countryDetails = countryData);
    }
  }

  angular.module('countryDetail')
    .component('countryDetailDashboard', {
      templateUrl: 'country-detail/country-detail-dashboard/country-detail-dashboard.component.html',
      controller: CountryDetailDashboardController
    });

}(angular));
