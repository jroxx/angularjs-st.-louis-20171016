(function (angular, _) {
  'use strict';

  class DetailFetcher {

    constructor($http, promiseSleep) {
      this.$http = $http;
      this.promiseSleep = promiseSleep;
    }

    load(countryCode) {
      return this.$http.get('../demo-data/countries.json')
        .then(this.promiseSleep(1800)) // to make it visibly delay
        .then(response => response.data)
        .then(countries => _.find(countries, {countryCode: countryCode}));
    }
  }

  angular.module('countryDetail')
    .service('detailFetcher', DetailFetcher);

}(angular, window._));
