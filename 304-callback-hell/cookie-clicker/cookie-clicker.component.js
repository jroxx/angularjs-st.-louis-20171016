(function (angular) {
  'use strict';

  class CookieClickerController {
    constructor($timeout) {
      this.$timeout = $timeout;
      this.message = '';
      this.cookieVisible = false;
      this.clickCookie = () => undefined;
    }

    start() {
      var score = 0;
      this.message = 'Get Ready!!';
      this.cookieVisible = true;
      this.$timeout(() => {
        this.clickCookie = () => score++;
        this.message = 'Go!!';
        this.$timeout(() => {
          this.message = 'Keep Clicking!!';
          this.$timeout(() => {
            this.message = 'STOP!!';
            this.clickCookie = () => score--;
            this.$timeout(() => {
              this.clickCookie = () => undefined;
              this.message = 'Your score was: ' + score;
            }, 1000);
          }, 5000);
        }, 1000);
      }, 1000);
    }
  }

  angular.module('app')
    .component('cookieClicker', {
      templateUrl: 'cookie-clicker/cookie-clicker.component.html',
      controller: CookieClickerController
    });

}(angular));
