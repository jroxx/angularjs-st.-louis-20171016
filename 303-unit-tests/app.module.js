(function (angular) {
  'use strict';

  angular.module('app', [])
    .constant('BASEAPIURL', 'http://mysite.com')
    .value('closeToPi', 3.14159);

}(angular));
