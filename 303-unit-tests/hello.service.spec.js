/* eslint strict: 0 */

describe('The exampleApp module', () => {
  beforeEach(module('app'));

  describe('hello', () => {
    let hello;
    beforeEach(inject(_hello_ => hello = _hello_));

    it('should say hello', () => {
      // write a failing test first
      const result = 'Hi Joe!';
      expect(hello.sayHello('Joe')).toEqual(result);
    });
  });
});
