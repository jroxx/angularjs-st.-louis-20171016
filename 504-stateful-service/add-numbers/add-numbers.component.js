(function (angular) {
  'use strict';

  class AddNumbersController {

    constructor(stateful) {
      this.values = {
        b: 5
      };
      this.sharedValues = stateful.sharedValues;
    }

    add() {
      const a = parseInt(this.sharedValues.a);
      const b = parseInt(this.values.b);
      return a + b;
    }
  }

  angular.module('app')
    .component('addNumbers', {
      templateUrl: 'add-numbers/add-numbers.component.html',
      controller: AddNumbersController
    });

}(angular));
