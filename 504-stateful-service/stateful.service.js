(function (angular) {
  'use strict';

  class Stateful {
    constructor() {
      this.sharedValues = { a: 1 };
    }
  }

  angular.module('app')
    .service('stateful', Stateful);

}(angular));
