(function (angular) {
  'use strict';

  class AbcPageController {
    constructor(dataFeed) {
      this.start = () => dataFeed.start();
      this.stop = () => dataFeed.stop();
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
