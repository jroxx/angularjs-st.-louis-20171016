(function (angular) {
  'use strict';

  class DataFeed {

    constructor($interval, stateful) {
      this.$interval = $interval;
      this.stateful = stateful;
      this.intervalIDs = [];
    }

    start() {
      // If you use bare setInterval, you will need to call $apply.
      // setInterval(() => {
      //   this.stateful.sharedValues.a =
      //   parseInt(this.stateful.sharedValues.a) + 1;
      //   this.$rootScope.$apply();
      // }, 2000);
      this.intervalIDs.push(this.$interval(() => {
        console.log(this.stateful.sharedValues);
        this.stateful.sharedValues.a =
          parseInt(this.stateful.sharedValues.a) + 1;
      }, 500));
    };

    stop() {
      this.intervalIDs.forEach(id => this.$interval.cancel(id));
      this.intervalIDs = [];
    };
  }

  angular.module('app')
    .service('dataFeed', DataFeed);

}(angular));
