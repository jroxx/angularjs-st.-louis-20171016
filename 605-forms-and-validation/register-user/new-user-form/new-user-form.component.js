(function (angular) {
  'use strict';


  class FormController {
    constructor() {
      this.user = undefined;
    }

    submit(form) {
      if (form.$valid) {
        console.log(this.user); // or post it?
        this.user = {};
        form.$setPristine();
        form.$setUntouched();
      }
    };
  }

  angular.module('registerUser')
    .component('newUserForm', {
      templateUrl: 'register-user/new-user-form/new-user-form.component.html',
      controller: FormController
    });

}(angular));
