(function (angular) {
  'use strict';

  class InjectionExampleController {

    // Consider the parameter list in the following line:
    constructor($log, $timeout, $interval, $location) {
      this.counter = 1;
      this.url = $location.absUrl();

      const incCounter = () => this.counter++;
      $interval(incCounter, 400);

      $timeout(() => $log.info('Hello World'), 1000);
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: InjectionExampleController
    });

}(angular));
