(function (angular) {
  'use strict';

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html'
    });

}(angular));
