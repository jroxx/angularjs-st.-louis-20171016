(function (angular) {
  'use strict';

  class FirstCookieClickerController {
    constructor($timeout) {
      this.$timeout = $timeout;
      this.message = '';
      this.cookieVisible = false;
      this.clickCookie = () => undefined;
    }

    start() {
      var score = 0;
      this.message = 'Get Ready!!';
      this.cookieVisible = true;
      this.$timeout(1000)
        .then(() => {
          this.clickCookie = () => score++;
          this.message = 'Go!!';
          return this.$timeout(1000);
        })
        .then(() => {
          this.message = 'Keep Clicking!!';
          return this.$timeout(5000);
        })
        .then(() => {
          this.message = 'STOP!!';
          this.clickCookie = () => score--;
          return this.$timeout(1000);
        })
        .then(() => {
          this.clickCookie = () => undefined;
          this.message = 'Your score was: ' + score;
        });
    }
  }

  angular.module('app')
    .component('firstCookieClicker', {
      templateUrl: 'first-cookie-clicker/first-cookie-clicker.component.html',
      controller: FirstCookieClickerController
    });

}(angular));
