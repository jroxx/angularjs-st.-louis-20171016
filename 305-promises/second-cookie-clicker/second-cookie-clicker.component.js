(function (angular) {
  'use strict';

  class SecondCookieClickerController {
    constructor($timeout) {
      this.$timeout = $timeout;
      this.message = '';
      this.cookieVisible = false;
      this.clickCookie = () => undefined;
    }

    start() {
      var score = 0;
      this.message = 'Get Ready!!';
      this.cookieVisible = true;

      const go = () => {
        this.clickCookie = () => score++;
        this.message = 'Go!!';
      };

      const keepClicking = () => {
        this.message = 'Keep Clicking!!';
      };

      const stop = () => {
        this.message = 'STOP!!';
        this.clickCookie = () => score--;
      };

      const showScore = () => {
        this.clickCookie = () => undefined;
        this.message = 'Your score was: ' + score;
      };

      this.$timeout(1000)
        .then(go)
        .then(() => this.$timeout(1000))
        .then(keepClicking)
        .then(() => this.$timeout(5000))
        .then(stop)
        .then(() => this.$timeout(1000))
        .then(showScore);
    }
  }

  angular.module('app')
    .component('secondCookieClicker', {
      templateUrl: 'second-cookie-clicker/second-cookie-clicker.component.html',
      controller: SecondCookieClickerController
    });

}(angular));
