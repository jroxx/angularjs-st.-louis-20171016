(function (angular) {
  'use strict';

  const aFewCountries =
    [
      {
        'countryCode': 'AD',
        'countryName': 'Andorra',
        'currencyCode': 'EUR',
        'population': 84000,
        'capital': 'Andorra la Vella',
        'continentName': 'Europe',
        'continent': 'EU',
        'areaInSqKm': 468,
        'languages': 'ca',
        'geonameId': '3041565'
      },
      {
        'countryCode': 'AE',
        'countryName': 'United Arab Emirates',
        'currencyCode': 'AED',
        'population': 4975593,
        'capital': 'Abu Dhabi',
        'continentName': 'Asia',
        'continent': 'AS',
        'areaInSqKm': 82880,
        'languages': 'ar-AE,fa,en,hi,ur',
        'geonameId': '290557'
      },
      {
        'countryCode': 'AF',
        'countryName': 'Afghanistan',
        'currencyCode': 'AFN',
        'population': 29121286,
        'capital': 'Kabul',
        'continentName': 'Asia',
        'continent': 'AS',
        'areaInSqKm': 647500,
        'languages': 'fa-AF,ps,uz-AF,tk',
        'geonameId': '1149361'
      }];

  class CountryListController {
    constructor() {
      this.countryList = aFewCountries;
    }
  }

  angular.module('app')
    .component('countryList', {
      templateUrl: 'country-list/country-list.component.html',
      controller: CountryListController
    });

}(angular));
