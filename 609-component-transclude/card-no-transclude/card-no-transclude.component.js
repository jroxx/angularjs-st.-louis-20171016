(function (angular) {
  'use strict';

  angular.module('app')
    .component('cardNoTransclude', {
      templateUrl: 'card-no-transclude/card-no-transclude.component.html'
    });
}(angular));
