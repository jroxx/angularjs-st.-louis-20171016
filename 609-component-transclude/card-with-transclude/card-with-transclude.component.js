(function (angular) {
  'use strict';

  angular.module('app')
    .component('cardWithTransclude', {
      transclude: true,
      templateUrl: 'card-with-transclude/card-with-transclude.component.html'
    });

}(angular));
