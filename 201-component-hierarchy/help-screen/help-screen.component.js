(function (angular) {
  'use strict';

  angular.module('app')
  .component('helpScreen', {
    templateUrl: 'help-screen/help-screen.component.html'
  });

}(angular));
