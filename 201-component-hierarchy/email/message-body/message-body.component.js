(function (angular) {
  'use strict';

  angular.module('app')
    .component('messageBody', {
      templateUrl: 'email/message-body/message-body.component.html'
    });

}(angular));
