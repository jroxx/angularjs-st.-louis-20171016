
(function (angular) {
  'use strict';

  angular.module('app')
    .component('emailScreen', {
      templateUrl: 'email/email-screen.component.html'
    });
}(angular));
