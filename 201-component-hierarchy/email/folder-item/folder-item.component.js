(function (angular) {
  'use strict';

  angular.module('app')
    .component('folderItem', {
      templateUrl: 'email/folder-item/folder-item.component.html'
    });

}(angular));
