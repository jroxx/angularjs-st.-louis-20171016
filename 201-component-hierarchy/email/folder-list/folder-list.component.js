(function (angular) {
  'use strict';

  angular.module('app')
    .component('folderList', {
      templateUrl: 'email/folder-list/folder-list.component.html'
    });

}(angular));
