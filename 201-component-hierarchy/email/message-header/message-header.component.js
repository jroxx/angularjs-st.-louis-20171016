(function (angular) {
  'use strict';

  angular.module('app')
    .component('messageHeader', {
      templateUrl: 'email/message-header/message-header.component.html'
    });

}(angular));
