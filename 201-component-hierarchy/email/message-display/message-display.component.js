(function (angular) {
  'use strict';

  angular.module('app')
    .component('messageDisplay', {
      templateUrl: 'email/message-display/message-display.component.html'
    });

}(angular));
