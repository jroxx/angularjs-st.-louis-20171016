(function (angular) {
  'use strict';

  angular.module('app', ['ngRoute'])
    .config($routeProvider => {
      $routeProvider
        .when('/help', {
          template: '<help-screen></help-screen>'
        })
        .when('/email', {
          template: '<email-screen></email-screen>'
        })
        .otherwise('/email');
    });

}(angular));

