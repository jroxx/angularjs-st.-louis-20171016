(function (angular) {
  'use strict';

  class AbcPageController {
    constructor() {
      this.val = '';
    }

    getSet(newVal) {
      return arguments.length ? this.val = newVal : this.val;
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
