(function (angular) {
  'use strict';

  class CountryDetailDashboardController {
    constructor($routeParams) {
      this.countryCode = $routeParams.countryCode;
    }
  }

  angular.module('countryDetail')
    .component('countryDetailDashboard', {
      templateUrl: 'country-detail/country-detail-dashboard/country-detail-dashboard.component.html',
      controller: CountryDetailDashboardController
    });

}(angular));
