(function (angular) {
  'use strict';

  const roomData = [
    { name: 'kitchen', contents: ['dishes', 'table', 'chairs'] },
    { name: 'bedroom', contents: ['bed', 'lamp'] }
  ];

  class AbcPageController {
    constructor() {
      this.n = 0;
      this.todos  = ['Eat Breakfast', 'Walk Dog', 'Breathe'];
      this.rooms = roomData;
    }

    increment() {
      this.n++;
      this.todos.push(`Task ${this.todos.length + 1}`);
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });
}(angular));
