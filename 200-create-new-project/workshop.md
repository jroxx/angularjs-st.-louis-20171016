# Workshop for this step

Starting in this step, please start your own project, then keep
extending it in many of the future workshops.

1. Create a new folder in "Learn", a sibling to all the numbered
   directories, to hold your project.
2. Create a new index.html
3. Set up ng-app
4. Add the angular library
5. Create Javascript files following a similar pattern as you have seen.
6. Use your own names though - "abc-page" refers to Angular Boot Camp,
   not to your application.
7. Create a module
8. Create a component
9. Put static HTML in its template
10. Use it from the index
