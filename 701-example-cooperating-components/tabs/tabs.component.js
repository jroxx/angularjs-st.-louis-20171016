(function (angular) {
  'use strict';

  class MyTabsController {
    constructor() {
      this.panes = [];
    }

    select(pane) {
      angular.forEach(this.panes, p => p.selected = false);
      pane.selected = true;
    }

    addPane(pane) {
      if (this.panes.length === 0) {
        this.select(pane);
      }
      this.panes.push(pane);
    }
  }

  angular.module('app')
    .component('myTabs', {
      controller: MyTabsController,
      templateUrl: 'tabs/tabs.component.html',
      transclude: true
    });

}(angular));
