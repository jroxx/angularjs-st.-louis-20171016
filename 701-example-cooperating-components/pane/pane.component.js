(function (angular) {
  'use strict';

  class MyPaneController {
    $onInit() {
      this.tabsCtrl.addPane(this);
      console.log(this);
    }
  }

  angular.module('app')
    .component('myPane', {
      controller: MyPaneController,
      templateUrl: 'pane/pane.component.html',
      transclude: true,
      require: {
        tabsCtrl: '^myTabs'
      },
      bindings: {
        title: '@'
      },
    });

}(angular));
