(function (angular) {
  'use strict';

  class UserData {
    processLogin(loginPromise) {
      loginPromise.then(() => {
        console.log('login processed');
      });
    };
  }

  angular.module('app')
    .service('userData', UserData);

}(angular));
