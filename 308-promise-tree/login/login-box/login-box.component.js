(function (angular) {
  'use strict';

  class LoginBoxController {
    constructor(login, userData) {
      this.loginService = login;
      this.userData = userData;
      this.loginStatus = false;
      this.loginResultPromise;
    }

    login() {
      // get a promise
      this.loginResultPromise = this.loginService.login();

      // Update view with status from promise
      this.loginResultPromise.then(result => this.loginStatus = result);

      // Go get some other data
      this.loginResultPromise.then(result => {
        if (result) {
          console.log('id: ', result.id);
          return this.loginService.getSomeData().then((data) => {
            this.loginData = data;
            return data;
          });
        }
      }).then(res => {
        console.log('Promise for promise? ', res);
        console.log(res.then);
      });

      this.loginResultPromise.then(data => console.log(data));

      // pass it to a function (which calls .then on it)
      this.userData.processLogin(this.loginResultPromise);
    };
  }

  angular.module('login')
    .component('loginBox', {
      templateUrl: 'login/login-box/login-box.component.html',
      controller: LoginBoxController
    });

}(angular));
