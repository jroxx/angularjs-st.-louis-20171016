(function (angular) {
  'use strict';

  class Login {

    constructor($http) {
      this.userPromise = $http.get('/api/employees')
        .then(response => {
          this.currentUser = response.data[0];
          return this.currentUser;
        });

      this.dataPromise = $http.get('/api/colors')
        .then(res => res.data);
    }

    login() {
      return this.userPromise.then(currentUser => {
        console.log('data is: ', currentUser);
        // very naive login service
        if (currentUser.id === 1) {
          return currentUser;
        }
        return false;
      });
    };

    getSomeData() {
      return this.dataPromise;
    };
  }

  angular.module('login')
    .service('login', Login);

}(angular));
