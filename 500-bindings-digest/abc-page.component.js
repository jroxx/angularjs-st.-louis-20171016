(function (angular) {
  'use strict';

  class AbcPageController {
    constructor() {
      this.counter = 0;

      // How does Angular know about this relationship?
      this.address = '123 Main';
    }

    foo() {
      this.address = '456 Park';
    }

    count() {
      console.log('count called');

      // What happens if we change things as part of this?
      // console.log("count called", ++this.counter);

      // How soon will this converge?
      // if(this.counter < 5) this.counter++;

      // Never return the same thing twice
      // return this.counter++;

      // Return the same thing each time - still called
      return 42;
    };
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
