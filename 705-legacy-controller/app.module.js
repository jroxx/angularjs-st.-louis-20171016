(function (angular) {
  'use strict';

  const x = angular.module('app', []);

  // Note the function is named twice.
  x.controller('SomeController', function SomeController() {
    this.name = 'Jim';
  });

  function AnotherController() {
    this.company = 'ABC';
  }

  x.controller('AnotherController', AnotherController);

}(angular));

// Evolution of controller definition
// 1.0 - 1.2
// ng-controller="YeOldeController" {{x}}
// function YeOldeController($scope) {
//   $scope.x = 3
// }

// 1.0 - 1.2 w/ 'Dot Rule'
// ng-controller="YeLessOldeController" {{y.x}}
// function YeLessOldeController ($scope) {
//   $scope.y = {
//     x: 3
//   };
// }

// 1.3 - 1.5
// ng-controller="NewerController as y" {{y.x}}
// function NewerController() {
//   this.x = 3;
// }

// 1.3 - 1.5 w/ Element Directive
// Generally, ng-controller has fallen out of favor
// {{y.x}}
// function NewerControllerUsedInDDO() {
//   this.x = 3;
// }

// let ddo = {
//   controller: NewerControllerUsedInDDO,
//   controllerAs: 'y'
// };

// 1.5 - present
// {{$ctrl.x}}
// class ModernController {
//   constructor() {
//     this.x = 3;
//   }
// }
// let componentdo = {
//   controller: ModernController
// };

