(function (angular) {
  'use strict';

  // module with [] declares a module and its dependencies.
  // Does the order matter? Yes, if there is a conflict.
  angular.module('app', [
    'person',
    'wrapper',
    'math'
    // Note that there is no dependency on 'card' here.
  ]);

}(angular));
