(function (angular) {
  'use strict';

  angular.module('wrapper')
    .component('wrapperCard', {
      transclude: true,
      templateUrl: 'wrapper/wrapper-card/wrapper-card.component.html',
    });

}(angular));
