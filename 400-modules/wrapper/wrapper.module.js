(function (angular) {
  'use strict';

  // The following module contains all of the components and functionality
  // relating to a wrapper. In the future this could include services,
  // multiple styles of wrappers, directives that turn divs into wrappers, etc
  // for our example it only has one piece in it, the wrapper-card component.

  angular.module('wrapper', []);

}(angular));
