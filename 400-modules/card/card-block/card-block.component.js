(function (angular) {
  'use strict';

  class CardBlockController {
    constructor() {
      this.roll = Math.floor(Math.random() * 10);
    }
  }

  angular.module('card')
    .component('cardBlock', {
      templateUrl: 'card/card-block/card-block.component.html',
      controller: CardBlockController
    });

}(angular));
