(function (angular) {
  'use strict';

  class MathWidgetController {
    constructor() {
      this.add = (a, b) => parseFloat(a) + parseFloat(b);
    }
  }

  angular.module('math')
    .component('mathWidget', {
      templateUrl: 'math/math-widget/math-widget.component.html',
      controller: MathWidgetController
    });

}(angular));
