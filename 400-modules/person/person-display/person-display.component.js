(function (angular) {
  'use strict';

  class PersonDisplayController {
    constructor() {
      this.name = 'Jim';
      this.deck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }
  }
  angular.module('person')
    .component('personDisplay', {
      templateUrl: 'person/person-display/person-display.component.html',
      controller: PersonDisplayController
    });

}(angular));
