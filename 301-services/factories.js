(function (angular) {
  'use strict';

  /* Javascript constructor functions look like this:
  function Foo() {
    this.prop1 = "eee";
  }

  const s = new Foo();

  const f = { prop1: "eee" };
  */


  // class MyService {
  //   sayHello(name) {
  //     return "Hi " + name + "!";
  //   }

  //   fullGreeting(n) {
  //     return "Greetings;" + mys.sayHello(n);
  //   };
  // }

  function myFactory(/* DI */) {
    // factory returns an object
    // you can run some code before
    function sayHello(name) {
      return 'Hi ' + name + '!';
    }
    function fullGreeting(n) {
      return 'Greetings;' + sayHello(n);
    }
    return {
      sayHello: sayHello,
      fullGreeting: fullGreeting
    };
  }

  function justSayHello(/* DI */) {
    return function (name) {
      return 'Hi ' + name + '!';
    };
  }

  angular.module('app')
    .factory('myFactory', myFactory)
    .factory('justSayHelloFactory', justSayHello);

}(angular));
