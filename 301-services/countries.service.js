(function (angular) {
  'use strict';

  class Countries {
    getList() {
      return ['Russia', 'Canada', 'United States', 'China'];
    }
  }

  angular.module('app')
    .service('countries', Countries);

}(angular));
