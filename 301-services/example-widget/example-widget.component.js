(function (angular) {
  'use strict';

  class ExampleWidgetController {
    constructor(hello, myFactory, justSayHelloFactory,
      BASEAPIURL, closeToPi) {

      this.greeting1 = hello.sayHello('Joe');
      this.greeting2 = myFactory.sayHello('Joe');
      this.greeting3 = justSayHelloFactory('Joe');
      this.url = BASEAPIURL;
      this.n = closeToPi;
    }
  }

  angular.module('app')
    .component('exampleWidget', {
      templateUrl: 'example-widget/example-widget.component.html',
      controller: ExampleWidgetController
    });

}(angular));
