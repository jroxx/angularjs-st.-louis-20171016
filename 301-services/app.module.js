(function (angular) {
  'use strict';

  /*
    Services -> injectable
    Ways to make Services:
    .constant
    .value
    .service
    .factory
  */

  const obj = { x: 4 };

  angular.module('app', [])
    .constant('BASEAPIURL', 'http://mysite.com')
    .value('closeToPi', 3.14159)
    .constant('obj', Object.freeze(obj));

}(angular));
