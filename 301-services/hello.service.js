(function (angular) {
  'use strict';

  class Hello {
    constructor(/* DI */) {
    }

    sayHello(name) {
      return 'Hi ' + name + '!';
    };

    fullGreeting(name) {
      return 'Greetings;' + this.sayHello(name);
    };
  }

  angular.module('app')
    .service('hello', Hello);

}(angular));
