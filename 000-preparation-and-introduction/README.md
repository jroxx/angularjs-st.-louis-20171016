# Angular Boot Camp - Learning Materials

http://angularbootcamp.com/

## Welcome

Welcome to the Oasis Digital Angular Boot Camp.

While we wait for others to arrive you can get a jumpstart by entering this URL
in your browser to download the class materials:

Setup summary:

1. Follow the prep instructions, if you haven't already:
   https://angularbootcamp.com/prep/
2. Download this ZIP file (copy paste URL to your browser):
   https://angularbootcamp.com/learn.zip
3. Extract the zip
4. Find the directory containing the results, i.e. the directory
   containing all the 00- 01- etc directories.
5. Open the containing directory in your IDE.
6. Navigate to that directory at the command line.
7. `npm install` - or await instructor tips.
7. `npm start`
8. Open in your browser: http://localhost:8080

-----------------------------------------------------------------------------------

## learn.zip - curriculum / example files

This is a standalone, ready to use set of files, that we will use
throughout class. The only tools truly necessary for this portion are
a text editor and any web server, but Node.js is strongly
recommended.

## Development Web Server Needed

You need to serve these files via a Web server; it will not work
acceptably to load them directly from "file" URLs.

**Important**: Serve the "learn" directory as your "web root". You
will not need to restart you server as we move from one step to the
next, and each example will be able to use common library and data
files.

## NPM Install

The setup summary above points out the `npm install` command. NPM will
install live-server and various other files useful in class,
downloading them from NPM's online repositories.

`npm start` live-server, which conveniently includes "live reload" in
the box. It will automatically inject pages it serves with a bit of
JavaScript to access a web socket to automatically reload the page
from the files change. You will not have to click refresh in the
browser as you make changes.

## Recommended: Visual Studio Code

As of early 2017, Visual Studio Code appears to have the most complete
developer assistance. This uses TypeScript d.ts files, even though
AngularJS 1.x is not written in TypeScript and neither are these
examples.

## Cloud9 Backup Plan

If you have great difficulty getting this running on your own computer,
you can get started on Cloud9. See the file `README_ONLINE_CLOUD9.md`.

## Agenda

The agenda varies over time; your instructor will adjust it to match
the needs of the class and the ongoing changes in the AngularJS
ecosystem. Here is a rough idea of the overall agenda:

* Basics of AngularJS - core vocabulary to build components
* Structuring an AngularJS application
* Development / build tooling
* Advanced and varied features (that are okay to learn later)
* Examples, details, surrounding ecosystem

## Workshops

The instructor may have changes to the workshops, as our curriculum is
under continuous improvement and is adjusted for the needs of each
class.
