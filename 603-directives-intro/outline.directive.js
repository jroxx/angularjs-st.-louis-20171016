(function (angular) {
  'use strict';

  angular.module('app')
    .directive('outline', () => {

      // element is already wrapped in a jqlite object
      const link = (scope, element, attrs) => {
        element.css({
          border: '1px solid green'
        });
      };

      return {
        restrict: 'A',
        link
      };

    });
}(angular));
