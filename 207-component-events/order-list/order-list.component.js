(function (angular) {
  'use strict';

  class OrderListController {
    select(newOrder) {
      console.log( 'select called' );
      
      //Defensive code example
      if ( this.selectOrder ) {
        this.selectOrder({
          event: newOrder
        });
      } else {
        throw new Error( 'select-order needs to be defined' );
      }
    }
  }

  angular.module('app')
    .component('orderList', {
      templateUrl: 'order-list/order-list.component.html',
      bindings: {
        orders: '<',
        selectOrder: '&'
      },
      controller: OrderListController
    });

}(angular));
