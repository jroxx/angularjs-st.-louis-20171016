(function (angular) {
  'use strict';

  angular.module('app')
    .component('orderDetails', {
      templateUrl: 'order-details/order-details.component.html',
      bindings: {
        sod: '<selectedOrderDetails'
      }
    });

}(angular));
