(function (angular) {
  'use strict';

  class Backend {
    loadTheData() {
      return ['Madagascar', 'Canada'];
    };
  }

  angular.module('app')
    .service('backend', Backend);

}(angular));
