(function (angular) {
  'use strict';

  class Countries {
    constructor(backend) {
      this.backend = backend;
    }

    getList() {
      return this.backend.loadTheData();
    }
  }

  angular.module('app')
    .service('countries', Countries);

}(angular));
