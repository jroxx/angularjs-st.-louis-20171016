(function (angular) {
  'use strict';

  /*
    Services -> injectable
    Ways to make Services:
    .constant
    .value
    .service
    .factory
  */

  angular.module('app', [])
    .constant('BASEAPIURL', 'http://mysite.com')
    .value('closeToPi', 3.14159);

}(angular));
