(function (angular) {
  'use strict';

  class CountryListController {
    constructor(countries) {
      this.list = countries.getList();
    }
  }

  angular.module('app')
    .component('countryList', {
      templateUrl: 'country-list/country-list.component.html',
      controller: CountryListController
    });

}(angular));
