(function (angular) {
  'use strict';

  class ExampleController {
    constructor(answerApi) {
      answerApi.loadData()
        .then(result => this.answer = result)
        .catch(err => this.err = err);
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: ExampleController
    });

}(angular));
