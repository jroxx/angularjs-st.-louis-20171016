(function (angular) {
  'use strict';

  angular.module('app', [])
    .config($httpProvider =>
      // Register an interceptor via an anonymous factory function
      $httpProvider.interceptors.push((/* DI here */) => ({
        'request': config => {
          config.headers['X-SECRET-HEADER'] = 'g76dg34f764';
          return config;
        },
        'response': response => {
          // process response here
          return response;
        }
      }))
    );

}(angular));
