(function (angular) {
  'use strict';

  // A service hosted by Oasis Digital for this example:
  const serviceUrl = 'https://jira-item-create.appspot.com/auth-example';

  class AnswerApi {
    constructor($http) {
      this.$http = $http;
    }

    loadData() {
      return this.$http.get(serviceUrl)
        .then(d => d.data);
    };
  }

  angular.module('app')
    .service('answerApi', AnswerApi);

}(angular));
