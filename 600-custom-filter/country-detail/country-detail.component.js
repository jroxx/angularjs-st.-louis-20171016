(function (angular) {
  'use strict';

  class CountryDetailController {
    constructor($http) {
      $http.get('../demo-data/usa.json')
        .then(response => this.country = response.data);
    }
  }

  angular.module('app')
    .component('countryDetail', {
      templateUrl: 'country-detail/country-detail.component.html',
      controller: CountryDetailController
    });

}(angular));
