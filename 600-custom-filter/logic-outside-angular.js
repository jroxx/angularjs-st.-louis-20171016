(function (window) {
  'use strict';

  // Plain JS file, no Angularism here.
  // For simplicity, this uses no module system at all,
  // instead putting this function on the global (window) object.

  function reverseString(d) {
    // console.log("reverse filter", d);
    if (d && d.split) {
      return d.split('').reverse().join('');
    }
  };

  window.reverseString = reverseString;
}(window));
