(function (angular) {
  'use strict';

  function add(addOneFilter) {
    return (a, b) => {
      if (angular.isNumber(a) && angular.isNumber(b)) {
        let result = a;
        for (let x = 0; x < b; x++) {
          result = addOneFilter(result);
        }
        return result;
      }
      return NaN;
    };
  }

  angular.module('common')
    .filter('add', add);

}(angular));
