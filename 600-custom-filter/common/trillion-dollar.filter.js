(function (angular) {
  'use strict';

  function trillionDollar($filter) {
    return input => $filter('currency')(input / 1000000000000) + ' trillion';
  }
  // const trillionDollar =
  //   $filter =>
  //     input =>
  //       $filter('currency')(input / 1000000000000) + ' trillion';

  angular.module('common')
    .filter('trillionDollar', trillionDollar);

}(angular));
