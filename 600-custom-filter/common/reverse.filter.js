(function (angular, reverseString) {
  'use strict';

  angular.module('common')
    .filter('reverse', () => reverseString);

}(angular, window.reverseString));
