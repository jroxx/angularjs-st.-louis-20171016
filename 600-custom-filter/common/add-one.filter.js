(function (angular) {
  'use strict';

  function addOne() {
    return input => {
      if (angular.isNumber(input)) {
        return input + 1;
      }
      return NaN;
    };
  }

  angular.module('common')
    .filter('addOne', addOne);

}(angular));
