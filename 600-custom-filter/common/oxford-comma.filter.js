(function (angular) {
  'use strict';

  function oxfordComma() {
    // This one takes an array of strings and returns a comma seperated string
    return input => {
      if (angular.isArray(input)) {
        const arr = angular.copy(input);
        const last = arr.splice(-1);
        return arr.join(', ') + ', and ' + last;
      }
      return input;
    };
  }

  angular.module('common')
    .filter('oxfordComma', oxfordComma);

}(angular));
