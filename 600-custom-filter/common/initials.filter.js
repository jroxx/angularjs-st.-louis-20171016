(function (angular) {
  'use strict';

  function initials() {
    // setup code could happen before function return

    return input => {
      // What happens if we don't check for input?
      if (input && angular.isString(input)) {
        // New York City
        const words = input.split(' ');
        // ['New', 'York', 'City']
        words.forEach((word, index) => {
          words[index] = word.slice(0, 1);
        });
        // ['N', 'Y', 'C']
        return words.join('');
        // NYC
      }
      return input;
    };
  }

  angular.module('common')
    .filter('initials', initials);

}(angular));
