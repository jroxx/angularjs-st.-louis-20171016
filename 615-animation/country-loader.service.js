(function (angular) {
  'use strict';

  function unwrapData(response) {
    return response.data;
  }

  class CountryLoader {
    constructor($http) {
      this.$http = $http;
    }

    getCountryList() {
      return this.$http.get('../demo-data/countries.json').then(unwrapData);
    }

    getCountryCodes() {
      return this.$http.get('../demo-data/countries.json').then(unwrapData).then(countries => countries.map(c => c.countryCode));
    }
  }

  angular.module('app')
    .service('countryLoader', CountryLoader);

}(angular));
