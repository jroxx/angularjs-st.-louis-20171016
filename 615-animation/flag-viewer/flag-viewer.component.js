(function (angular) {
  'use strict';

  class FlagViewerController {
    constructor(countryLoader) {
      countryLoader.getCountryCodes().then(codes => {
        this.codes = codes;
        this.select(codes[0]);
      });
    }

    select(code) {
      this.selectedCode = code;
    }
  }

  angular.module('app')
    .component('flagViewer', {
      templateUrl: 'flag-viewer/flag-viewer.component.html',
      controller: FlagViewerController
    });

}(angular));
