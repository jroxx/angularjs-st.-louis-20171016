(function (angular) {
  'use strict';

  angular.module('app', [
    'ngRoute',
    'countryAnimations'
  ])
    .config(function ($routeProvider) {
      $routeProvider
        .when('/countries', {
          template: '<country-list></country-list>'
        })
        .when('/flags', {
          template: '<flag-viewer></flag-viewer>'
        })
        .otherwise({
          redirectTo: '/countries'
        });
    });

}(angular));
