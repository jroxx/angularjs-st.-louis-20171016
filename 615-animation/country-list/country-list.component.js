(function (angular) {
  'use strict';

  class CountryListController {
    constructor(countryLoader) {
      this.orderProp = 'countryName';
      countryLoader.getCountryList().then(countries => this.countries = countries);
    }
  }

  angular.module('app')
    .component('countryList', {
      templateUrl: 'country-list/country-list.component.html',
      controller: CountryListController
    });

}(angular));
