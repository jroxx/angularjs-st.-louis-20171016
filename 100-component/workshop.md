# Workshop for this step

For the first few workshops, edit/add files here in this directory.

1. Add a second component: add JS to app.js, and add a HTML template
   file for it.
2. Use it in index.html.
3. Experiment with the name of the component vs. how it is used in
   index.html.
