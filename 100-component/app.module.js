(function (ng) {
  'use strict';

  const appModule = ng.module('app', []);

  appModule.component('abcDemo', {
    templateUrl: 'abc-demo.html'
  });

  appModule.component( 'abcNewComponent', {
    templateUrl: 'abc-new-component.html'
  });
  
}(angular));

// IIFE = Immediately Invoked Function Expression
// * Inform some IDEs that global access is intentional.
// * Document use of globals.
// * Protect from accidental overwrite.
// * Rename locally ('ng')
