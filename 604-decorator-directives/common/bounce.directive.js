(function (angular) {
  'use strict';

  angular.module('common')
    .directive('bounce', $interval => {

      const link = (scope, element, attrs) => {
        let n = 0;
        const intervalId = $interval(() => {
          n += 0.1;
          element.css({
            'transform': 'rotate(' + Math.sin(n) * 5 + 'deg)' //
          });
        }, 25);

        element.on('$destroy', () => {
          $interval.cancel(intervalId);
        });
      };

      return {
        restrict: 'A',
        link
      };

    });
}(angular));
