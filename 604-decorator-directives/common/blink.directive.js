(function (angular) {
  'use strict';

  angular.module('common')
    .directive('blink', $interval => {
      const link = (scope, element, attrs) => {
        let on = true;
        const intervalId = $interval(() => {
          console.log('interval tick');
          on = !on;
          element.css({
            display: on ? 'inline-block' : 'none'
          });
        }, 500);

        element.on('$destroy', () => {
          console.log('Destroying interval');
          $interval.cancel(intervalId);
        });
      };

      return {
        restrict: 'A',
        link
      };
    });

}(angular));
