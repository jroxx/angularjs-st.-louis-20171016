(function (angular) {
  'use strict';

  class AbcPageController {

    constructor() {
      this.currentTimeFormat = 'HH:MM:SS';
    }

  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
