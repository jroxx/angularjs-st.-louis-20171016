(function (angular) {
  'use strict';

  class AbcPageController {
    constructor() {
      this.count = 0;
      this.text = '';
      this.inputText = '';
    }

    increment() {
      this.count++;
    }

    setText(text) {
      this.increment();
      this.text = text;
    }

    myClick( myEvent ) {
      console.log( 'myEvent', myEvent );
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
