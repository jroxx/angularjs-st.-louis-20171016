# Workshop for this step

1. Add another event function to this component.
2. Add a button to the HTML that uses that event function.
3. Look in the AngularJS documentation for ng-mouseover, then use it in
   this component.
