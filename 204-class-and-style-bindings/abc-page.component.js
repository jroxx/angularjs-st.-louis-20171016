(function (angular) {
  'use strict';

  class AbcPageController {
    constructor() {
      this.n = 1;
    }

    increment() {
      this.n++;
    }

    classFor(x) {
      let myClass = 'round-border';
      if (x > 7) {
        myClass += ' extra-class';
      } 

      return myClass;
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });
}(angular));
