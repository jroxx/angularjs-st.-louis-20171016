(function (angular) {
  'use strict';

  function extractData(response) {
    return response.data;
  }

  function extractFilmTitle(response) {
    return response.data.title;
  }

  class SwapiService {
    constructor($http, $q, $timeout, $log, filmNamesCache) {
      this.$http = $http;
      this.$q = $q;
      this.$timeout = $timeout;
      this.$log = $log;
      this.filmNamesCache = filmNamesCache;
    }

    augmentWithFilmNames(personResponse) {
      const people = personResponse.results;
      const cache = {}; // URL -> promise of the title

      return this.filmNamesCache.getFilmNames()
        .catch(err => [
          'Christmas Special 1',
          'Christmas Special 2',
          'Christmas Special 3',
          'Christmas Special 4',
          'Christmas Special 5',
          'Christmas Special 6',
          'Christmas Special 7',
          'Christmas Special 8',
          'Christmas Special 9',
        ])
        .then(names => people.map(p => {
          p.filmNames = p.films.map(filmUrl => {
            let segments = filmUrl.split('/'),
              id = segments[segments.length - 2];
            return names[id - 1];
          });
          return p;
        }));
    }

    listPeople() {
      return this.$http.get('http://swapi.co/api/people/')
        .then(extractData)
        .then(this.augmentWithFilmNames.bind(this));
    }

    listStarships() {
      return this.$http.get('http://swapi.co/api/starships/');
    }
  }

  class FilmNamesCache {
    constructor($http) {
      this.filmNames = $http.get('http://swapi.co/api/films')
        .then(res => res.data.results.sort((a, b) => a.url > b.url).map(f => f.title))
    }
    getFilmNames() {
      return this.filmNames;
    }
  }


  angular.module('app')
    .service('swapi', SwapiService)
    .service('filmNamesCache', FilmNamesCache);

}(angular));
