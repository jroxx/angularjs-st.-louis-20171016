(function (angular) {
  'use strict';

  class MovieListController {
    constructor(swapi) {

      swapi.listPeople()
        .then(data => this.people = data)
        .catch(err => console.log('err:', err));
    }
  }

  angular.module('app')
    .component('movieList', {
      templateUrl: 'movie-list/movie-list.component.html',
      controller: MovieListController
    });

}(angular));
