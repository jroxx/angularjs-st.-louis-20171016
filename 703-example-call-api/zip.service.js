(function (angular) {
  'use strict';

  class ZipService {
    constructor($http) {
      this.$http = $http;
    }

    lookup(zipcode) {
      return this.$http.get('http://api.zippopotam.us/us/' + zipcode)
        .then(response => response.data);
    };
  }

  angular.module('app')
    .service('zips', ZipService);

}(angular));
