(function (angular) {
  'use strict';

  class ZipSearchController {
    constructor(zips) {

      this.getPlaceDetails = pcode => {
        if (pcode.match(/\d{5}/)) {
          zips.lookup(pcode)
            .then(data => this.locationDetails = data);
        }
      };
    }
  }

  angular.module('app')
    .component('zipSearch', {
      templateUrl: 'zip-search/zip-search.component.html',
      controller: ZipSearchController
    });

}(angular));
