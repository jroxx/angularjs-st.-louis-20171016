(function (angular) {
  'use strict';

  angular.module('app', [])

    // This is necessary to avoid a problem with E2E testing,
    // irreletant to normal use.
    .config(function ($qProvider) {
      $qProvider.errorOnUnhandledRejections(false);
    });

}(angular));
