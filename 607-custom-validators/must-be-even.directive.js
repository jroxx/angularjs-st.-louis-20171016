(function (angular) {
  'use strict';

  // A custom validator looks like any other directive,
  // other than requiring ngModel.
  angular.module('app')
    .directive('mustBeEven', () => {

      const link = (scope, elm, attrs, modelController) => {
        modelController.$validators.even = (modelValue, viewValue) => {
          return modelController.$isEmpty(modelValue) ||
            parseInt(modelValue) % 2 === 0;
        };
      };

      return {
        require: 'ngModel',
        restrict: 'A',
        link
      };
    });

}(angular));
