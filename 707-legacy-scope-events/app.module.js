(function (angular) {
  'use strict';

  function ListenerController($scope) {
    const lc = this;
    $scope.$on('nameChange', (event, newName) => {
      // This can cause trouble in some IEs
      if (Math.random() < .75) {
        console.log(event);
        lc.childName = newName;
      } else {
        $scope.$broadcast('no', 'Joe');
        lc.childName = 'Joe';
      }
    });
  }

  function EmitController($scope) {
    const ec = this;
    ec.name = 'John';
    ec.changeName = function () {
      $scope.$emit('nameChange', ec.name);
    };
    $scope.$on('no', (e, v) => ec.name = v);
  }

  angular.module('app', [])
    .controller('ListenerController', ListenerController)
    .controller('EmitController', EmitController);

}(angular));
