(function (angular) {
  'use strict';

  angular.module('app')
    .component('abcDemo', {
      templateUrl: 'abc-demo.component.html'
    });

}(angular));
