(function (angular) {
  'use strict';

  class NavBarController {
    constructor($rootScope) {
      this.navigating = false;

      $rootScope.$on('$routeChangeStart',
        () => this.navigating = true);

      $rootScope.$on('$routeChangeSuccess',
        () => this.navigating = false);
    }
  }

  angular.module('app')
    .component('navBar', {
      templateUrl: 'nav-bar/nav-bar.component.html',
      controller: NavBarController
    });

}(angular));
