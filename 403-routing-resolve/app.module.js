(function (angular) {
  'use strict';

  angular.module('app', ['ngRoute', 'welcome', 'country'])
    .config($routeProvider => {
      $routeProvider.
        when('/welcome', {
          template: '<welcome-page></welcome-page>'
        }).
        when('/country/:id', {
          template: '<country-details country="$resolve.country"></country-details>',
          resolve: {
            // DI is available in a resolve function.
            country: function ($route, detailFetcher) {
              // $routeParams is not updated until the route is resolved;
              // use $route.current.params instead
              return detailFetcher.load($route.current.params.id);
            }
          }
        }).
        otherwise({
          redirectTo: '/welcome'
        });
    });

}(angular));
