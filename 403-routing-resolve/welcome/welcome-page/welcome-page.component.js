(function (angular) {
  'use strict';

  angular.module('welcome')
    .component('welcomePage', {
      templateUrl: 'welcome/welcome-page/welcome-page.component.html'
    });

}(angular));

