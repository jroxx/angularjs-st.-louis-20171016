(function (angular) {
  'use strict';

  angular.module('country')
    .component('countryDetails', {
      templateUrl: 'country/country-details/country-details.component.html',
      bindings: {
        country: '='
      }
    });
}(angular));
