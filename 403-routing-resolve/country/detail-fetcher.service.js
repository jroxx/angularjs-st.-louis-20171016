(function (angular) {
  'use strict';

  class DetailFetcher {

    constructor($http, promiseSleep) {
      this.$http = $http;
      this.promiseSleep = promiseSleep;
    }

    load(countryCode) {
      return this.$http.get('../demo-data/' + countryCode + '.json')
        .then(this.promiseSleep(1800)) // to make it visibly delay
        .then(response => response.data);
    }
  }

  angular.module('country')
    .service('detailFetcher', DetailFetcher);

}(angular));
