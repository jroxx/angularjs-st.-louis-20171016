(function (angular) {
  'use strict';

  angular.module('app')
    .factory('promiseSleep', $timeout =>
      ms =>
        value =>
          $timeout(() => value, ms));

}(angular));
