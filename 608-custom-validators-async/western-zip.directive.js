(function (angular) {
  'use strict';

  const url = 'https://api.zippopotam.us/us/';

  angular.module('app')
    .directive('westernZip', ($q, $timeout, $http) => {

      function link(scope, elm, attrs, ctrl) {
        ctrl.$asyncValidators.westernZip = (modelValue, viewValue) => {
          if (ctrl.$isEmpty(modelValue) || modelValue.length !== 5) {
            return $q.reject('not valid zip'); // Empty == not valid
          } else {
            return $timeout(() => { }, 3000)
              .then(() => $http.get(url + modelValue))
              .then(response => {
                const longitude = response.data.places[0].longitude;
                if (longitude > -90) {
                  return $q.reject();
                }
                //   return $q.resolve();
                // more idiomatic to return nothing here
              });
          }
        };
      }

      return {
        require: 'ngModel',
        link
      };
    });

}(angular));
