(function (angular) {
  'use strict';

  function AppSettings() {
    let setting1 = 3;
    return {
      setSetting1: function (s) {
        setting1 = s;
      },
      $get: function ($http /* Dependency injection */) {
        const data = {
          setting1: setting1,
          setting2: 'foo',
        };
        return {
          setSetting2: function (s) {
            data.setting2 = s;
          },
          getFreshData: function () {
            data.data = $http.get('data.json');
          }
        };
      }
    };
  }

  angular.module('app')
    .provider('appSettings', AppSettings);


    // class Foo {

    // }

  // angular.module('app')
  //   .value('foo', new Foo())
  //   .service('foo', Foo)
  //   .factory('foo', () => new Foo())
  //   .provider('foo', () => ({ $get: () => new Foo() }))

}(angular));
