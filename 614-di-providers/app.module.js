(function (angular) {
  'use strict';

  angular.module('app', [])
    .config(appSettingsProvider => {
      // Comment this out to see the default settings
      appSettingsProvider.setSetting1('bar');
    });

}(angular));
