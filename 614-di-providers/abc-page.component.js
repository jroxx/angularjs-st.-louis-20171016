(function (angular) {
  'use strict';

  class AbcPageController {
    constructor(appSettings) {
      this.settings = appSettings;
      appSettings.getFreshData();
      appSettings.myCustomSetting = 'baz';
    }

  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
