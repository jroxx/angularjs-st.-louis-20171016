(function (angular) {
  'use strict';

  function extractDataLength(v) {
    return v.data.length;
  }

  class ColorsService {
    constructor($http) {
      this.$http = $http;
    }

    get() {
      return this.$http.get('/api/colors')
        .then(extractDataLength);
    };
  }

  angular.module('app')
    .service('colors', ColorsService);

}(angular));
