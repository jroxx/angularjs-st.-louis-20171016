(function (angular) {
  'use strict';

  class AbcPageController {
    constructor($q, colors, promiseSleep) {
      // Another way to create a promise
      const p1 = $q((resolve, reject) => {
        this.go1 = function go1() {
          resolve(1);
        };
        this.stop = function stop() {
          reject('oops');
        };
      });

      // We've seen this way before:
      const p2 = $q.resolve(5875);

      // Async services return promises:
      const colorsPromise = colors.get().then(promiseSleep(5000));

      const all = $q.all([p1, colorsPromise, p2]);

      all.then(answers => this.message = `The answers are: ${answers.join(', ')}`)
        .catch(e => this.oops = e);

      // Also possible to pass the error fn as a second then param.
      // all.then(answers => this.message = `The answers are: ${answers.join(', ')`, e => this.oops = e);
    }

  }

  angular.module('app', ['promise_utils'])
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
