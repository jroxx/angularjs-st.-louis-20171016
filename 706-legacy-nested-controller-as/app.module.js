(function (angular) {
  'use strict';

  function FirstController() {
    this.name = 'Barry';
  }

  angular.module('app', [])
    .controller('FirstController', FirstController);

}(angular));
