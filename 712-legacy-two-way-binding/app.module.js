angular.module('app', [])
  .component('abcPage', {
    template: `
      <input ng-model="$ctrl.foo">
      <user-entry foo="$ctrl.foo"></user-entry>
    `
  })
  .component('userEntry', {
    template: `
      <input ng-model="$ctrl.foo">
    `,
    bindings: {
      foo: '='
    }
  })
