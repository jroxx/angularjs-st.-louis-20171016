(function (angular) {
  'use strict';

  class RedController {
    constructor() {
      this.message = 'Hello Red World!';
    }
  }

  angular.module('app')
    .component('red', {
      templateUrl: 'red/red.component.html',
      controller: RedController
    });

}(angular));
