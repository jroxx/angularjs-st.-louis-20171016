(function (angular) {
  'use strict';

  angular.module('colorGroup')
    .component('yellowOne', {
      templateUrl: 'color-group/yellow-one/yellow-one.component.html'
    });
}(angular));
