(function (angular) {
  'use strict';

  function myFunction(params) {
    return parseInt(params.id) < 100 ? '<yellow-one></yellow-one>' : '<yellow-two></yellow-two>';
  }

  angular.module('colorGroup', ['ngRoute'])
    .config($routeProvider => $routeProvider
      .when('/green', {
        template: '<green></green>',
        myCustomName: 'Green'
      }))
    .config($routeProvider => $routeProvider
      .when('/yellow/:id', {
        template: myFunction,  // called with route parameters
        myCustomName: 'Yellow'
      }));
}(angular));
