(function (angular) {
  'use strict';

  angular.module('colorGroup')
    .component('yellowTwo', {
      templateUrl: 'color-group/yellow-two/yellow-two.component.html'
    });

}(angular));
