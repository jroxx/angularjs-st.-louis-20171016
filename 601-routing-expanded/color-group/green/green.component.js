(function (angular) {
  'use strict';

  class GreenController {
    constructor() {
      this.message = 'Hello Green World!';
    }
  }

  angular.module('colorGroup')
    .component('green', {
      templateUrl: 'color-group/green/green.component.html',
      controller: GreenController
    });

}(angular));
