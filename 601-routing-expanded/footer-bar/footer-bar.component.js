(function (angular) {
  'use strict';

  angular.module('app')
    .component('footerBar', {
      templateUrl: 'footer-bar/footer-bar.component.html'
    });
}(angular));
