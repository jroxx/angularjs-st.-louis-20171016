(function (angular) {
  'use strict';

  angular.module('app', ['ngRoute', 'colorGroup'])
    .config($routeProvider => {
      $routeProvider
        .when('/blue', {
          template: '<blue></blue>',
          myCustomName: 'Blue 1'
        })
        .when('/bLUe', {
          template: '<h1>I\'m bLUe</h1>',
          myCustomName: 'Blue 2'
        })
        .when('/red', {
          template: '<red></red>',
          caseInsensitiveMatch: true,
          myCustomName: 'Red'
        })
        .otherwise({
          redirectTo: '/blue'
        });

      // Can also be set globally:
      // $routeProvider.caseInsensitiveMatch = true;
    });
}(angular));
