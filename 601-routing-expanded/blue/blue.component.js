(function (angular) {
  'use strict';

  class BlueController {
    constructor() {
      this.message = 'Hello Blue World!';
    }
  }

  angular.module('app')
    .component('blue', {
      templateUrl: 'blue/blue.component.html',
      controller: BlueController
    });

}(angular));
