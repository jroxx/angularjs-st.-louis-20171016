(function (angular) {
  'use strict';

  angular.module('app')
    .component('headerBar', {
      controller: function ($route) {
        this.getTitle = () =>
          $route.current && $route.current.myCustomName;
      },
      templateUrl: 'header-bar/header-bar.component.html'
    });
}(angular));
