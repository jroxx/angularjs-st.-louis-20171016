(function (angular) {
  'use strict';

  // Change this value to put more bindings on the page
  const bindingsCount = 100;

  class AbcPageController {
    constructor() {
      this.address = '123 Main';
      this.ngIf = true;
      this.ngShow = true;

      this.boxes = [];
      for (let j = 0; j < bindingsCount; j++) {
        this.boxes.push({ n: j });
      }
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
