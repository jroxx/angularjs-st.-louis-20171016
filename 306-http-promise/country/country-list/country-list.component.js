(function (angular) {
  'use strict';

  class NaiveCountryController {
    constructor($http) {
      this.list = [];
      // Talking to the backend from UI code - bad!
      $http.get('../demo-data/countries.json')
        .then(response => this.list = response.data);
    }
  }

  // Most Angular experts recommend calling $http from service layer
  class BetterCountryController {
    constructor(countries) {
      this.list = [];

      countries.getList()
        .then(countryList => this.list = countryList);
    }
  }

  angular.module('country')
    .component('countryList', {
      templateUrl: 'country/country-list/country-list.component.html',
      controller: NaiveCountryController
      // controller: BetterCountryController
    });

}(angular));
