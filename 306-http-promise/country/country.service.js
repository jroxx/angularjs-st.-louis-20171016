(function (angular) {
  'use strict';

  class CountryService {
    constructor($http, $timeout) {
      this.$http = $http;
      this.$timeout = $timeout;
    }

    getList() {
      // Functions that return asynchronously return a promise.
      return this.$http.get('../demo-data/countries.json')
        .then(response => response.data);
    }

    // Cancels request if longer that 5 seconds
    getListWithTimeout() {
      return this.$http
        .get('../demo-data/countries.json', {
          timeout: this.$timeout(function (req) { }, 5000)
        })
        .then(response => response.data);
    }
  }

  angular.module('country')
    .service('countries', CountryService); // Note the case for each

}(angular));
