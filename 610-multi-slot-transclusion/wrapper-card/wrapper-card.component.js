(function (angular) {
  'use strict';

  angular.module('app')
    .component('wrapper', {
      transclude: {
        head: '?header',
        body: 'content'
      },
      templateUrl: 'wrapper-card/wrapper-card.component.html'
    });
}(angular));
