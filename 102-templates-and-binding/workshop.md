# Workshop for this step

1. Add several data fields to the component already in this code.
2. Use data binding in the template
3. Add another component (JS and HTML), with fields
4. Try misspelling various aspects of the data bindings, and observe
   which emit errors and which don't.
