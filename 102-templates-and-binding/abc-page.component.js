(function (angular) {
  'use strict';
  //es6 class
  class AbcPageController {
    constructor() {
      this.company = 'ABC';
      this.name = 'Jim';
    }
  }

  //es5 style
  // function AbcPageController() {
  //   this.company = 'ABC';
  //   this.name = 'Jim';
  // }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
