( function( angular ) {

  class MyNewThingController {
    constructor() {
      this.myNumber = 8;
    }
  }

  angular.module( 'app' )
    .component('abcMyNewThing', {
      templateUrl: 'abc-my-new-thing.component.html',
      controller: MyNewThingController
    })
}( angular ));
