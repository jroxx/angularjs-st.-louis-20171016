(function (angular) {
  'use strict';

  const DATA_URL = '../demo-data/countries.json';

  class CountryService {
    constructor($http, $timeout) {
      this.$http = $http;
      this.$timeout = $timeout;
    }

    getList() {
      return this.$http.get(DATA_URL)
        .then(response => response.data);
    }

    getFirst4() {
      return this.getList()
        .then(response => response.slice(0, 4));
    }

    // Cancels request if longer that 5 seconds
    getListWithTimeout() {
      return this.$http
        .get(DATA_URL, {
          timeout: this.$timeout(function (req) { }, 5000)
        })
        .then(response => response.data);
    }
  }

  angular.module('country')
    .service('countries', CountryService); // Note the case for each

}(angular));
