/* eslint strict: 0 */

describe('The country module', () => {
  beforeEach(module('country'));

  const DATA_URL = '../demo-data/countries.json';

  const dataList = [
    {
      'countryCode': 'AD',
      'countryName': 'Andorra',
      'currencyCode': 'EUR',
      'population': 84000,
      'capital': 'Andorra la Vella',
      'continentName': 'Europe',
      'continent': 'EU',
      'areaInSqKm': 468,
      'languages': 'ca',
      'geonameId': '3041565'
    },
    {
      'countryCode': 'AE',
      'countryName': 'United Arab Emirates',
      'currencyCode': 'AED',
      'population': 4975593,
      'capital': 'Abu Dhabi',
      'continentName': 'Asia',
      'continent': 'AS',
      'areaInSqKm': 82880,
      'languages': 'ar-AE,fa,en,hi,ur',
      'geonameId': '290557'
    },
    {
      'countryCode': 'AF',
      'countryName': 'Afghanistan',
      'currencyCode': 'AFN',
      'population': 29121286,
      'capital': 'Kabul',
      'continentName': 'Asia',
      'continent': 'AS',
      'areaInSqKm': 647500,
      'languages': 'fa-AF,ps,uz-AF,tk',
      'geonameId': '1149361'
    },
    {
      'countryCode': 'AG',
      'countryName': 'Antigua and Barbuda',
      'currencyCode': 'XCD',
      'population': 86754,
      'capital': 'St. John\'s',
      'continentName': 'North America',
      'continent': 'NA',
      'areaInSqKm': 443,
      'languages': 'en-AG',
      'geonameId': '3576396'
    },
    {
      'countryCode': 'AL',
      'countryName': 'Albania',
      'currencyCode': 'ALL',
      'population': 2986952,
      'capital': 'Tirana',
      'continentName': 'Europe',
      'continent': 'EU',
      'areaInSqKm': 28748,
      'languages': 'sq,el',
      'geonameId': '783754'
    },
    {
      'countryCode': 'AM',
      'countryName': 'Armenia',
      'currencyCode': 'AMD',
      'population': 2968000,
      'capital': 'Yerevan',
      'continentName': 'Asia',
      'continent': 'AS',
      'areaInSqKm': 29800,
      'languages': 'hy',
      'geonameId': '174982'
    },
    {
      'countryCode': 'AO',
      'countryName': 'Angola',
      'currencyCode': 'AOA',
      'population': 13068161,
      'capital': 'Luanda',
      'continentName': 'Africa',
      'continent': 'AF',
      'areaInSqKm': 1246700,
      'languages': 'pt-AO',
      'geonameId': '3351879'
    }];

  describe('countries service', function () {
    let countries, http;

    beforeEach(inject(function (_countries_, $httpBackend) {
      countries = _countries_;
      http = $httpBackend;

      http.whenGET(DATA_URL).respond(dataList);
    }));

    it('should retrieve some countries', function () {
      const firstGroup = dataList.slice(0, 4);

      http.expectGET(DATA_URL);

      countries.getFirst4().then(function (data) {
        expect(data.length).toEqual(4);
        expect(data).toEqual(firstGroup);
      });

      http.flush();
    });

    afterEach(function () {
      http.verifyNoOutstandingExpectation();
      http.verifyNoOutstandingRequest();
    });
  });
});
