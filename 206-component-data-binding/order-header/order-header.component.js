(function (angular) {
  'use strict';

  angular.module('app')
    .component('orderHeader', {
      templateUrl: 'order-header/order-header.component.html',
      bindings: {
        customer: '<customerName'
      }
    });

}(angular));
