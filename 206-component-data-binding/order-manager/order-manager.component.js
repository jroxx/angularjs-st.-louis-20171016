(function (angular) {
  'use strict';

  class OrderManagerController {
    constructor() {
      this.orderList = fakeApiOrderData();
      this.selectedOrder = undefined;
    }

    choose(i) {
      this.selectedOrder = this.orderList[i];
    }
  }

  angular.module('app')
    .component('orderManager', {
      templateUrl: 'order-manager/order-manager.component.html',
      controller: OrderManagerController
    });

  function fakeApiOrderData() {
    return [
      {
        'id': 123,
        'customer': 'Oasis Digital',
        'items': [
          {
            'quantity': 3,
            'description': 'Widgets'
          }, {
            'quantity': 2,
            'description': 'FooVase'
          }, {
            'quantity': 6,
            'description': 'BarMitzvah'
          }
        ]
      },
      {
        'id': 456,
        'customer': 'Paul',
        'items': [
          {
            'quantity': 1,
            'description': 'Sprockets'
          }, {
            'quantity': 26,
            'description': 'Spanners'
          }, {
            'quantity': 9,
            'description': 'Wizbangers'
          }
        ]
      }
    ];
  }
}(angular));

