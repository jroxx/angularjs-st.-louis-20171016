(function (angular) {
  'use strict';

  angular.module('app')
    .component('orderList', {
      templateUrl: 'order-list/order-list.component.html',
      bindings: {
        orders: '<'
      }
    });

}(angular));
