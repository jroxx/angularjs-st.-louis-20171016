(function (angular) {
  'use strict';

  angular.module('app')
    .component('orderItems', {
      templateUrl: 'order-items/order-items.component.html',
      bindings: {
        items: '<'
      }
    });

}(angular));
