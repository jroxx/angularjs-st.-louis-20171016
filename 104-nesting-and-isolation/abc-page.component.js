(function (angular) {
  'use strict';

  class AbcPageController {
    constructor() {
      this.count = 0;
    }

    increment() {
      this.count++;
    }

    setText(text) {
      this.increment();
      this.text = text;
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
