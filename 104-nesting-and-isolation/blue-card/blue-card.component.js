(function (angular) {
  'use strict';

  class BlueCardController {
    constructor() {
      this.x = Math.round(Math.random() * 10000);

    }
  }

  angular.module('app')
    .component('blueCard', {
      templateUrl: 'blue-card/blue-card.component.html',
      controller: BlueCardController
    });

}(angular));
