(function (angular) {
  'use strict';

  class PromiseController {
    constructor(promiseExamples) {
      promiseExamples.calculate()
        .then(data => this.answer = data);
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: PromiseController
    });

}(angular));
