(function (angular) {
  'use strict';

  const extractData = response => response.data;

  class PromiseService {

    constructor($http, $timeout, $interval, $q) {
      this.$http = $http;
      this.$timeout = $timeout;
      this.$interval = $interval;
      this.$q = $q;
    }

    calculate() {

      const p1 = this.$http.get('/api/shapes').then(extractData);

      const p2 = this.$timeout(() => 3, 2000);

      const p3 = this.$http.get('/api/colors').then(extractData);

      const p4 = this.$interval(() => 4, 500, 4);

      // Using more than one plain function together looks like this:
      // function aa() {}
      // function bb() { throw "444"}
      // function cc() {}
      // function dd() {
      //   aa();
      //   bb();
      //   cc();
      // }

      // Verbose, and serial:
      p1.then(v => {
        p2.then(v2 => {
          p3.then(v3 => {
            p4.then(v4 => {
              console.log('answer-serial: ', v, v2, v3, v4);
              // console.log('length of http responses: ', v3.length + v.length)
            });
          });
        });
      });

      // Concise, and "parallel".
      // Promise<any>[] -> Promise<any[]>
      
      return this.$q.all([p1, p2, p3, p4])
        .then(answers => {
          console.log('length of http responses: ', answers[0].length + answers[2].length);
          return answers;
        },
        e => {
          console.error('error from $q all', e);
        });
    };
  }

  angular.module('app')
    .service('promiseExamples', PromiseService);

}(angular));
