(function (angular) {
  'use strict';

  // $sanitize applied to ng-bind-html just by including the ngSanitize module
  angular.module('app', ['ngSanitize']);

}(angular));
