(function (angular) {
  'use strict';

  class AbcPageController {
    constructor($sce, $sanitize) {
      this.unsafeHtml =
        '<a onmouseover="this.textContent=\'dangerous!\'" href="http://twitter.com">Twitter</a>';
      this.trustedHtml = $sce.trustAsHtml(this.unsafeHtml);
      this.sanitized = $sanitize(this.unsafeHtml);
    }
  }

  angular.module('app')
    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
