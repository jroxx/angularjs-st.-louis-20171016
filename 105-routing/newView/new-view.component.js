( function( angular ) {
  angular.module( 'app' )
    .component( 'newView', {
      templateUrl: 'newView/new-view.component.html'
    })
}( angular ));
