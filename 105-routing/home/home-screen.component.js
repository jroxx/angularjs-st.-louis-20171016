(function (angular) {
  'use strict';

  angular.module('app')
    .component('homeScreen', {
      templateUrl: 'home/home-screen.component.html'
    });

}(angular));
