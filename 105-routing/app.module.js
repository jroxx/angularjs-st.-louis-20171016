(function (angular) {
  'use strict';

  // ngRoute is an AngularJS companion module - modules will be
  // explained in depth later
  angular.module('app', ['ngRoute'])
    // This is Dependency Injection - we will explain how AngularJS provides this
    // parameter later
    .config($routeProvider => {
      $routeProvider
        .when('/home', {
          template: '<home-screen></home-screen>'
        })
        .when('/hello', {
          template: '<hello-screen></hello-screen>'
        })
        .when( '/newView', {
          template: '<new-view></new-view>'
        });
    });

}(angular));

