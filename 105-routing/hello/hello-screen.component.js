(function (angular) {
  'use strict';

  angular.module('app')
    .component('helloScreen', {
      templateUrl: 'hello/hello-screen.component.html'
    });

}(angular));
