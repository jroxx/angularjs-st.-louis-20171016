( function( angular ) {

  angular.module( 'dmmApp' )
    .component( 'employeeList', {
      templateUrl: 'employee-list/employee-list.component.html',
      bindings: {
        employees: '<',
        searchTerm: '<',
        orderProp: '<sortProperty'
      }
    })

}( angular ));
