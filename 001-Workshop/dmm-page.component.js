( function( angular ) {

  class DmmPageController {
    constructor() {
      this.selectedButton = '';
      this.employees = [
        {
          "id": 1,
          "first_name": "Henry",
          "last_name": "Holmes",
          "email": "hholmes0@goodreads.com",
          "hours_worked": 29,
          "hourly_wage": 19
        },
        {
          "id": 2,
          "first_name": "Harold",
          "last_name": "Cox",
          "email": "hcox1@who.int",
          "hours_worked": 18,
          "hourly_wage": 11
        },
        {
          "id": 3,
          "first_name": "Brian",
          "last_name": "Garcia",
          "email": "bgarcia2@addthis.com",
          "hours_worked": 4,
          "hourly_wage": 17
        },
        {
          "id": 4,
          "first_name": "Patricia",
          "last_name": "Young",
          "email": "pyoung3@wix.com",
          "hours_worked": 47,
          "hourly_wage": 12
        },
        {
          "id": 5,
          "first_name": "Jose",
          "last_name": "Jacobs",
          "email": "jjacobs4@prweb.com",
          "hours_worked": 45,
          "hourly_wage": 12
        },
        {
          "id": 6,
          "first_name": "Rachel",
          "last_name": "Carter",
          "email": "rcarter5@t.co",
          "hours_worked": 34,
          "hourly_wage": 17
        },
        {
          "id": 7,
          "first_name": "Ashley",
          "last_name": "Reyes",
          "email": "areyes6@bing.com",
          "hours_worked": 60,
          "hourly_wage": 9
        },
        {
          "id": 8,
          "first_name": "Antonio",
          "last_name": "Cruz",
          "email": "acruz7@jigsy.com",
          "hours_worked": 38,
          "hourly_wage": 10
        },
        {
          "id": 9,
          "first_name": "Randy",
          "last_name": "Cook",
          "email": "rcook8@state.tx.us",
          "hours_worked": 22,
          "hourly_wage": 15
        },
        {
          "id": 10,
          "first_name": "Phillip",
          "last_name": "Long",
          "email": "plong9@un.org",
          "hours_worked": 39,
          "hourly_wage": 19
        },
        {
          "id": 11,
          "first_name": "Andrea",
          "last_name": "Simpson",
          "email": "asimpsona@is.gd",
          "hours_worked": 4,
          "hourly_wage": 10
        },
        {
          "id": 12,
          "first_name": "Tina",
          "last_name": "Rice",
          "email": "triceb@wisc.edu",
          "hours_worked": 18,
          "hourly_wage": 20
        },
        {
          "id": 13,
          "first_name": "Larry",
          "last_name": "Foster",
          "email": "lfosterc@wikia.com",
          "hours_worked": 26,
          "hourly_wage": 19
        },
        {
          "id": 14,
          "first_name": "Philip",
          "last_name": "Hall",
          "email": "phalld@vkontakte.ru",
          "hours_worked": 32,
          "hourly_wage": 10
        },
        {
          "id": 15,
          "first_name": "Annie",
          "last_name": "Gilbert",
          "email": "agilberte@ucsd.edu",
          "hours_worked": 57,
          "hourly_wage": 20
        },
        {
          "id": 16,
          "first_name": "Wanda",
          "last_name": "Myers",
          "email": "wmyersf@gravatar.com",
          "hours_worked": 14,
          "hourly_wage": 12
        },
        {
          "id": 17,
          "first_name": "Robert",
          "last_name": "Palmer",
          "email": "rpalmerg@usnews.com",
          "hours_worked": 30,
          "hourly_wage": 12
        },
        {
          "id": 18,
          "first_name": "Judith",
          "last_name": "King",
          "email": "jkingh@auda.org.au",
          "hours_worked": 39,
          "hourly_wage": 14
        },
        {
          "id": 19,
          "first_name": "Sarah",
          "last_name": "Mendoza",
          "email": "smendozai@fc2.com",
          "hours_worked": 16,
          "hourly_wage": 20
        },
        {
          "id": 20,
          "first_name": "Marilyn",
          "last_name": "Watson",
          "email": "mwatsonj@google.com.hk",
          "hours_worked": 19,
          "hourly_wage": 19
        },
        {
          "id": 21,
          "first_name": "Kelly",
          "last_name": "Payne",
          "email": "kpaynek@bluehost.com",
          "hours_worked": 12,
          "hourly_wage": 16
        },
        {
          "id": 22,
          "first_name": "Craig",
          "last_name": "Willis",
          "email": "cwillisl@google.pl",
          "hours_worked": 43,
          "hourly_wage": 13
        },
        {
          "id": 23,
          "first_name": "Samuel",
          "last_name": "Russell",
          "email": "srussellm@pinterest.com",
          "hours_worked": 41,
          "hourly_wage": 8
        },
        {
          "id": 24,
          "first_name": "Andrew",
          "last_name": "Willis",
          "email": "awillisn@deviantart.com",
          "hours_worked": 48,
          "hourly_wage": 14
        },
        {
          "id": 25,
          "first_name": "Ann",
          "last_name": "Davis",
          "email": "adaviso@merriam-webster.com",
          "hours_worked": 25,
          "hourly_wage": 13
        },
        {
          "id": 26,
          "first_name": "Katherine",
          "last_name": "Martinez",
          "email": "kmartinezp@npr.org",
          "hours_worked": 42,
          "hourly_wage": 18
        },
        {
          "id": 27,
          "first_name": "Larry",
          "last_name": "Hanson",
          "email": "lhansonq@google.ru",
          "hours_worked": 23,
          "hourly_wage": 17
        },
        {
          "id": 28,
          "first_name": "David",
          "last_name": "Collins",
          "email": "dcollinsr@deliciousdays.com",
          "hours_worked": 19,
          "hourly_wage": 13
        },
        {
          "id": 29,
          "first_name": "Marilyn",
          "last_name": "Fernandez",
          "email": "mfernandezs@xrea.com",
          "hours_worked": 3,
          "hourly_wage": 20
        },
        {
          "id": 30,
          "first_name": "Julia",
          "last_name": "Mendoza",
          "email": "jmendozat@chron.com",
          "hours_worked": 31,
          "hourly_wage": 18
        },
        {
          "id": 31,
          "first_name": "Anna",
          "last_name": "Bryant",
          "email": "abryantu@de.vu",
          "hours_worked": 56,
          "hourly_wage": 16
        },
        {
          "id": 32,
          "first_name": "Brandon",
          "last_name": "Ellis",
          "email": "bellisv@biglobe.ne.jp",
          "hours_worked": 36,
          "hourly_wage": 8
        },
        {
          "id": 33,
          "first_name": "Sharon",
          "last_name": "Hunt",
          "email": "shuntw@printfriendly.com",
          "hours_worked": 41,
          "hourly_wage": 13
        },
        {
          "id": 34,
          "first_name": "Juan",
          "last_name": "Mills",
          "email": "jmillsx@miibeian.gov.cn",
          "hours_worked": 11,
          "hourly_wage": 11
        },
        {
          "id": 35,
          "first_name": "Diane",
          "last_name": "Murray",
          "email": "dmurrayy@tmall.com",
          "hours_worked": 6,
          "hourly_wage": 10
        },
        {
          "id": 36,
          "first_name": "Harry",
          "last_name": "Coleman",
          "email": "hcolemanz@ocn.ne.jp",
          "hours_worked": 21,
          "hourly_wage": 8
        }
      ];
    }

    selectButton( selectedButton ) {
      this.selectedButton = selectedButton;
      if ( this.selectedButton === 'FAQ' ) {
        return 'pulse';
      }
    }
  }

  angular.module( 'dmmApp' )
    .component( 'dmmPage', {
      templateUrl: 'dmm-page.component.html',
      controller: DmmPageController
    });
    
}( angular ) );
