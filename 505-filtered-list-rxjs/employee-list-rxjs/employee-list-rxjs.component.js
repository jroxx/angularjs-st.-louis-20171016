(function (angular, _, Rx) {
  'use strict';

  class EmployeeListController {
    constructor(employeeLoader) {
      this.nameFilter = '';
      this.sort = 'last_name';
      this.filteredList = undefined;
      this.selectedEmployee = undefined;
      this.selectedId = undefined;
      this.selectedId$ = new Rx.Subject();

      this.nameFilter$ = new Rx.BehaviorSubject(this.nameFilter);
      this.sort$ = new Rx.BehaviorSubject(this.sort);
      this.filteredList$ = Rx.Observable.combineLatest(
        this.nameFilter$.switchMap(nameFilter =>
          Rx.Observable.fromPromise(
            employeeLoader.getList(nameFilter))),
        this.sort$,
        (list, sort) => _.sortBy(list, sort)
      );

      this.selectedEmployee$ = this.selectedId$
        .switchMap(id =>
          Rx.Observable.fromPromise(
            employeeLoader.getDetails(id)));
    }

    // More fluent Observable style is to do these in the template instead:

    nameFilterChanged() {
      this.nameFilter$.next(this.nameFilter);
    }

    sortChanged() {
      this.sort$.next(this.sort);
    }

    employeeClicked(id) {
      this.selectedId$.next(id);
    }

    // The following can be eliminated, with
    // additional minor AngularJS-Observable integration.

    $onInit() {
      // Make data available to the template.
      this.filteredListSub = this.filteredList$.subscribe(x => this.filteredList = x);
      this.selectedIdSub = this.selectedId$.subscribe(x => this.selectedId = x);
      this.selectedEmployeeSub = this.selectedEmployee$.subscribe(x => this.selectedEmployee = x);
    }

    $onDestroy() {
      // Clean up subscriptions
      this.filteredListSub.unsubscribe();
      this.selectedIdSub.unsubscribe();
      this.selectedEmployeeSub.unsubscribe();
    }
  }

  angular.module('app')
    .component('employeeListRxjs', {
      templateUrl: 'employee-list-rxjs/employee-list-rxjs.component.html',
      controller: EmployeeListController
    });

}(angular, _, Rx));
