(function (angular, _) {
  'use strict';

  class EmployeeListController {
    constructor(employeeLoader) {
      this.nameFilter = '',
        this.sort = 'last_name';
      this.filteredList = undefined;
      this.selectedEmployee = undefined;
      this.selectedId = undefined;

      this.employeeLoader = employeeLoader;
      this.nameFilterChanged(); // inital data load
    }

    nameFilterChanged() {
      this.employeeLoader.getList(this.nameFilter)
        .then(list => _.sortBy(list, this.sort))
        .then(x => this.filteredList = x);
    }

    sortChanged() {
      this.filteredList = _.sortBy(this.filteredList, this.sort);
    }

    employeeClicked(id) {
      this.selectedId = id;
      this.employeeLoader.getDetails(id)
        .then(x => this.selectedEmployee = x);
        // You can partially fix jitter, the following is a step towards that
        // .then(emp => emp.id === this.selectedId ? this.selectedEmployee = emp : '');
    }
  }

  angular.module('app')
    .component('employeeListPromise', {
      templateUrl: 'employee-list-promise/employee-list-promise.component.html',
      controller: EmployeeListController
    });

}(angular, _));
