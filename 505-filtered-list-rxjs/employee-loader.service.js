(function (angular) {
  'use strict';

  const API_URL = '/api';

  // Configure the amount of latency and jitter to simulate
  const API_LATENCY = 300;
  const API_JITTER = 3000;  // set to 3000 to see trouble easily.

  class EmployeeLoader {
    constructor($http, randomPromiseDelay) {
      this.$http = $http;
      this.randomPromiseDelay = randomPromiseDelay;
    }

    getList(searchText) {
      return this.$http.get(API_URL + '/employees',
        {
          params: {
            'q': searchText,
            '&_limit': 20
          }
        })
        .then(response => response.data)
        .then(this.randomPromiseDelay(API_LATENCY, API_JITTER));
    }

    getDetails(employeeId) {
      return this.$http.get(`${API_URL}/employees/${employeeId}`)
        .then(response => response.data)
        .then(this.randomPromiseDelay(API_LATENCY, API_JITTER));
    }
  }

  angular.module('app')
    .service('employeeLoader', EmployeeLoader); // Note the case for each

}(angular));
