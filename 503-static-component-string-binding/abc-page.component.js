(function (angular) {
  'use strict';

  class AbcPageController {
    constructor() {
    }
  }

  angular.module('app', [])

    .component('abcPage', {
      templateUrl: 'abc-page.component.html',
      controller: AbcPageController
    });

}(angular));
