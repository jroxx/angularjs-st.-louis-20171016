(function (angular) {
  'use strict';

/*
  // Component definition
  &  Output/event binding
  <  Input/one way binding
  @  static string binding
  =  two way binding (not recommended)

  At point of use (for = and < bindings):
  ::  one time binding
*/

  angular.module('app')
    .component('messageBox', {
      templateUrl: 'message-box/message-box.html',
      bindings: {
        'title': '@title',
        'message': '@msg'
      }
    });

}(angular));
